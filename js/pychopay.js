function PychoPay() {
    this.payments = {};
    this.sumInputs = {};
    this.changeInputs = {};
    this.init();
}
PychoPay.prototype.defaults = {
    matrixSelector: '[data-pychopay]',
    rowSelector: 'tr[data-payer]'
};
PychoPay.prototype.init = function (options) {
    this.getData();
};
PychoPay.prototype.getData = function () {
    var matrix = document.querySelector(this.defaults.matrixSelector);
    var rows = matrix.querySelectorAll(this.defaults.rowSelector);

    for (var i = 0, j = rows.length; i < j; i++) {
        this.initPayer(rows[i]);
    }
};
PychoPay.prototype.initPayer = function (row) {
    var that = this;
    var payerKey = row.dataset['payer'];
    var paymentInputs = row.querySelectorAll('[data-payment][data-payee]');
    this.payments[payerKey] = {};
    this.payments[payerKey].sum = 0;
    this.payments[payerKey].cash = 0;
    this.payments[payerKey].change = 0;

    for (var i = 0, j = paymentInputs.length; i < j; i++) {
        var payeeKey = paymentInputs[i].dataset['payee'];
        that.payments[payerKey][payeeKey] = {
            payment: parseFloat(paymentInputs[i].dataset['payment']),
            payed: false
        }
        paymentInputs[i].addEventListener('change', function (event) {
            that.onPaymentToggle(event.target,that);
        });
    }
    this.sumInputs[payerKey] = row.querySelector('input[data-cash]');
    this.changeInputs[payerKey] = row.querySelector('input[data-change]');

    this.sumInputs[payerKey].addEventListener('change', function (event) {
        that.onPay(event.target,that);
    });
};
PychoPay.prototype.onPaymentToggle = function (element,that) {
    var payerKey = element.dataset['payer'];
    var payeeKey = element.dataset['payee'];
    var payed = element.checked;
    var price = that.payments[payerKey][payeeKey].payment

    that.payments[payerKey][payeeKey].payed = payed;

    that.payments[payerKey].sum += payed ? price : -price;
    that.recalculatePayer(payerKey);
};
PychoPay.prototype.onPay = function (element,that) {
    var payerKey = element.dataset['cash'];
    var cash = parseFloat(element.value);

    that.payments[payerKey].cash = cash;
    that.recalculatePayer(payerKey);
};
PychoPay.prototype.recalculatePayer = function(payerKey){
    var payer = this.payments[payerKey]
    payer.change = payer.cash - payer.sum;
    this.changeInputs[payerKey].value = payer.change;
}

