var PychotkaOrders = {
    groupId: null,
    lastId: 0,
    sum: 0,
    date: "",
    init: function (groupId) {
        var self = this;
        this.groupId = groupId;
        this.getOrders(true);
        this.initMenu();
        this.initPaidChecks();
        this.initEditButtons();
        setInterval(function () {
            self.getOrders(false);
        }, 10000);
    },
    initMenu: function () {
        $('.danie').on('click', function () {
            var nazwa = this.getAttribute('data-nazwa');
            var cena = this.getAttribute('data-cena');

            if ($('#bun-form').length){
                var selectedBun = $('#bun-form input[name=bun]:checked').val();
                nazwa += " - "+selectedBun;
            }

            if ($('#form-cena').val() === "") {
                $('#form-cena').val("0");
            }
            if ($('#form-zamowienie').val() !== "") {
                nazwa = $('#form-zamowienie').val() + "\n" + nazwa;
            }
            $('#form-zamowienie').val(nazwa);
            $('#form-cena').val(parseFloat($('#form-cena').val().replace(',', '.')) + parseFloat(cena));
        });
    },
    initPaidChecks: function () {
        var now = new Date();
        var midnight = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1, 0, 0, 0, 0);
        $('#wszystkie-zamowienia').on('click', '.done', function () {
            if (this.checked) {
                this.parentNode.classList.add("done");
                CM.set("done-" + this.parentNode.id, 1, midnight)
            } else {
                this.parentNode.classList.remove("done");
                CM.set("done-" + this.parentNode.id, 0, midnight)
            }
        });
    },
    initEditButtons: function () {
        $('#wszystkie-zamowienia').on('click', '.zamowienieEdycja', function () {
            $('#form-imienazwisko').val($('#' + this.parentNode.id + ' .zamowienieNazwisko')[0].innerHTML).attr('readonly', 'readonly');
            $('#form-cena').val($('#' + this.parentNode.id + ' .zamowienieSuma')[0].innerHTML.replace(',', '.'));
            $('#form-zamowienie').val($('#' + this.parentNode.id + ' .zamowienieTresc')[0].innerHTML.replace(/<br>/g,''));
            $('#form-komentarz').val($('#' + this.parentNode.id + ' .zamowienieKomentarz')[0].innerHTML.replace(/<br>/g,''));
        });
    },
    getOrders: function (initialRun) {
        var self = this;
        var r = new XMLHttpRequest();
        r.open("GET", "/?groupId=" + this.groupId + "&action=getOrders&lastId=" + this.lastId, true);
        r.onreadystatechange = function () {
            if (r.readyState != 4 || r.status != 200)
                return;
            var ordersData = JSON.parse(r.responseText);
            if (initialRun) {
                self.date = ordersData.date;
            } else if (ordersData.date !== self.date) {
                window.location.reload();
            }
            self.displayOrders(ordersData, !initialRun);
        };
        r.send();
    },
    displayOrders: function (ordersData, showNotifications) {
        var html = '';
        var zamawiajacy = '';
        this.sum += parseFloat(ordersData.sum);
        for (ord in ordersData.orders) {
            var order = ordersData.orders[ord];
            var changed = $('#zamowienie-' + order.uniqueId).length > 0;
            if (showNotifications && order.id > this.lastId) {
                this.showNotification(order, changed);
            }
            this.lastId = order.id;
            if (parseFloat(order.price) > 0) {
                zamawiajacy = order.name;
                order.order = order.order.replace(/\n/g, '<br>');
                order.comment = order.comment.replace(/\n/g, '<br>');
                order.done = CM.get('done-' + order.uniqueId) ? 'done' : '';
                html += ich.orderTmpl(order);
            }

            if (changed) {
                this.removeOldOrder(order);
            }
        }
        if (zamawiajacy.length > 0) {
            $('#zamawiajacy')[0].innerHTML = zamawiajacy;
        }
        this.displaySummary(ordersData);
        this.updateSum();
        $('#wszystkie-zamowienia')[0].innerHTML += html;
    },
    displaySummary: function (ordersData) {
        var htmlSummary = '';
        for (dish in ordersData.summary) {
            var summaryData = {
                dish: dish,
                count: ordersData.summary[dish],
                multiple: ordersData.summary[dish] > 1
            };
            htmlSummary += ich.summary(summaryData);
        }
        if (htmlSummary.length > 0) {
            $('#podsumowanie ul')[0].innerHTML = htmlSummary;
        }
    },
    showNotification: function (order, changed) {
        var text = ": nowe zamówienie";
        if (changed) {
            text = parseFloat(order.price) > 0 ? ": zmiana zamówienia" : ": jednak nie zmawiam";
        }
        if (notify.permissionLevel() === notify.PERMISSION_GRANTED) {
            notify.createNotification(order.name + text, {
                icon: 'images/chat.ico',
                tag: order.id
            });
        }
    },
    updateSum: function () {
        var sums = $('.suma');
        for (i in sums) {
            sums[i].innerHTML = ("" + this.sum.toFixed(2)).replace('.', ',');
        }
        $('#meter').attr('value', this.sum.toFixed(0));
    },
    removeOldOrder: function (order) {
        var price = $('#zamowienie-' + order.uniqueId + ' .zamowienieSuma')[0].innerHTML;
        price = parseFloat(price.replace(',', '.'));
        if (typeof price == 'number') {
            this.sum -= price;
        }
        $('#zamowienie-' + order.uniqueId).remove();
    }
}
