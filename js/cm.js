/*
 * CM.JS
 * A microlibrary to help you manage cookies.
 *
 * Tim Severien
 * http://timseverien.nl
 *
 * Copyright (c) 2013 Tim Severien
 * Released under the GPLv2 license.
 *
 */

!function(t){var e=new Date(0),n=t.JSON,r=n.stringify,i=encodeURI,o=decodeURI,c=function(t){try{return n.parse(t)}catch(e){}return t},u=function(t){return"object"==typeof t&&r?r(t):t},s={set:function(t,e,n,r,o){var c=i(t)+"="+i(u(e));n&&(n.constructor===Number?c+=";max-age="+n:n.constructor===String?c+=";expires="+n:n.constructor===Date&&(c+=";expires="+n.toUTCString())),c+=";path="+(r?r:"/"),o&&(c+=";domain="+o),document.cookie=c},setObject:function(t,e,n,r){for(var i in t)this.set(i,t[i],e,n,r)},get:function(t){return this.getObject()[t]},getObject:function(){var t,e=document.cookie.split(/;\s?/i),n={};for(var r in e)t=e[r].split("="),t.length<=1||(n[o(t[0])]=c(o(t[1])));return n},unset:function(t){document.cookie=t+"=; expires="+e.toUTCString()},clear:function(){var t=this.getObject();for(var e in t)this.unset(e)}};"function"==typeof define&&define.amd?define(function(){return s}):t.CM=s}(this);
