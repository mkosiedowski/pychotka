var Shoutbox = {
    groupId: null,
    lastId: 0,
    windowTitle: "",
    newMsgCount: 0,
    init: function (groupId) {
        this.groupId = groupId;
        this.windowTitle = window.document.title;
        this.getMessages(false);
        var self = this;
        setInterval(function () {
            self.getMessages(true);
        }, 2000);
        $("body").on('click', function () {
            self.newMsgCount = 0;
            self.changeTitle(0);
        });

    },
    getMessages: function (changeTitle) {
        var self = this;
        var r = new XMLHttpRequest();
        r.open("GET", "/?groupId=" + this.groupId + "&action=getShoutboxMessages&lastId=" + this.lastId, true);
        r.onreadystatechange = function () {
            if (r.readyState != 4 || r.status != 200)
                return;
            var shoutboxData = JSON.parse(r.responseText);
            self.displayMessages(shoutboxData, changeTitle);
        };
        r.send();
    },
    displayMessages: function (shoutboxData, changeTitle) {
        var html = '';
        if (changeTitle) {
            this.newMsgCount += shoutboxData.length;
        }
        for (msg in shoutboxData) {
            if (changeTitle && shoutboxData[msg].id > this.lastId && notify.permissionLevel() === notify.PERMISSION_GRANTED) {
                notify.createNotification(shoutboxData[msg].name + " napisał(a) na czacie pychotki", {
                    body: shoutboxData[msg].message,
                    icon: 'images/chat.ico',
                    tag: shoutboxData[msg].id
                });
            }
            html += ich.shoutboxmsg(shoutboxData[msg]);
            this.lastId = shoutboxData[msg].id;
        }
        if (this.newMsgCount > 0 && changeTitle) {
            this.changeTitle(this.newMsgCount);
        }
        $('#shoutboxMessages')[0].innerHTML += html;
        $('#shoutboxMessages')[0].scrollTop = $('#shoutboxMessages')[0].scrollHeight;
    },
    changeTitle: function (count) {
        window.document.title = this.windowTitle + (count > 0 ? " (" + count + ")" : "");
    }
}
