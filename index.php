<?php
require_once('./vendor/autoload.php');
date_default_timezone_set('Europe/Warsaw');
mb_internal_encoding("UTF-8");
$appController = new AppController();
header("X-Clacks-Overhead: GNU Terry Pratchett");
$action = filter_input(INPUT_GET, "action", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW & FILTER_FLAG_STRIP_HIGH);
try {
    $appController->run($action);
    extract($appController->getViewVariables());
    include("views/{$appController->getViewName()}.php");
} catch (Exception $ex) {
    include("views/ExceptionView.php");
}
