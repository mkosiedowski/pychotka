FROM php:7.2
ADD . /var/www/pychotka
RUN apt-get update --fix-missing \
    && apt-get install -y cron
RUN crontab /var/www/pychotka/crontab
RUN cron
WORKDIR /var/www/pychotka
EXPOSE 9999
CMD ["/bin/bash", "/var/www/pychotka/startup.sh"]
