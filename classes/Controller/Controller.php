<?php
namespace Controller;

abstract class Controller {
    private $viewName = "";
    private $viewVariables = array();

    public function run($action = "default") {
        $action = ucfirst($action);
        $method = "run$action";
        if (method_exists($this, $method)) {
            $this->$method();
        } else {
            throw new NoActionException("No action $method");
        }
    }
    
    abstract public function runDefault();
    
    public function getViewName() {
        if (empty($this->viewName)) {
            throw new NoViewException("View is not set");
        }
        return $this->viewName;
    }
    
    public function getViewVariables() {
        return (array)$this->viewVariables;
    }
    
    protected function setViewName($viewName) {
        $this->viewName = $viewName;
    }

    protected function setViewVariables($viewVariables) {
        $this->viewVariables = $viewVariables;
    }
    
    protected function setViewVariable($name, $value) {
        $this->viewVariables[$name] = $value;
    }
    
    protected function unsetViewVariable($name) {
        if (isset($this->viewVariables[$name])) {
            unset($this->viewVariables[$name]);
        }
    }

    protected function returnJSON($object) {
        header("Content-type: application/json; charset=utf-8");
        echo json_encode($object);
        exit;
    }
}
