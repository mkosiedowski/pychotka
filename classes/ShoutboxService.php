<?php

class ShoutboxService {

    /**
     * DB handle
     *
     * @var PDO
     */
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

    public function createDB() {
        $sql = "
	  CREATE TABLE IF NOT EXISTS shoutbox (
              id INTEGER PRIMARY KEY AUTOINCREMENT,
	      id_grupy TEXT,
	      imienazwisko TEXT,
	      data TEXT,
	      wiadomosc TEXT,
	      czas_dodania TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	  )";
        $foo = $this->db->exec($sql);
    }

    public function getMessages($date, $groupId, $lastId = 0) {
        $sql = "
	    SELECT * FROM shoutbox
	    WHERE data = :date
		AND id_grupy = :groupId
                AND id > :lastId
            ORDER BY czas_dodania
	";
        $query = $this->db->prepare($sql);
        $query->bindValue(':date', $date);
        $query->bindValue(':groupId', $groupId);
        $query->bindValue(':lastId', $lastId);
        $orders = array();
        $query->execute();
        while ($row = $query->fetch()) {
            $orders[] = new ShoutboxMessage($row['id'], $row['id_grupy'],
                    $row['imienazwisko'], $row['data'], $row['wiadomosc'], 
                    $row['czas_dodania']);
        }
        return $orders;
    }

    public function saveMessage(ShoutboxMessage $order) {
        $sql = "
            REPLACE INTO shoutbox (id_grupy, imienazwisko, data, wiadomosc)
            VALUES (:groupId, :imienazwisko, :now, :wiadomosc)
        ";
        $query = $this->db->prepare($sql);
        $query->bindValue(":groupId", $order->getGroupId());
        $query->bindValue(":imienazwisko", $order->getName());
        $query->bindValue(":wiadomosc", $order->getMessage());
        $query->bindValue(":now", $order->getDate());
        return $query->execute();
    }
    
}
