<?php

class OrderService {

    /**
     * DB handle
     *
     * @var PDO
     */
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

    public function createDB() {
        $sql = "
	  CREATE TABLE IF NOT EXISTS zamowienia (
              id INTEGER PRIMARY KEY AUTOINCREMENT,
	      id_grupy TEXT,
	      imienazwisko TEXT,
	      data DATE,
	      zamowienie TEXT,
	      komentarz TEXT,
	      cena FLOAT,
	      czas_dodania TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	      UNIQUE(id_grupy, imienazwisko, data)
	  )";
        $this->db->exec($sql);
    }

    public function getOrders($date, $groupId, $lastId = 0) {
        $sql = "
	    SELECT * FROM zamowienia
	    WHERE data = :date
		AND id_grupy = :groupId
		AND id > :lastId
            ORDER BY czas_dodania
	";
        $query = $this->db->prepare($sql);
        $query->bindValue(':date', $date);
        $query->bindValue(':groupId', $groupId);
        $query->bindValue(':lastId', $lastId);
        $orders = array();
        $query->execute();
        while ($row = $query->fetch()) {
            $orders[] = new Order($row['id'], $row['id_grupy'], $row['imienazwisko'], $row['zamowienie'], $row['cena'], $row['komentarz'], $row['data']);
        }
        return $orders;
    }

    public function saveOrder(Order $order) {
        $sql = "
            REPLACE INTO zamowienia (id_grupy, imienazwisko, data, zamowienie, komentarz, cena)
            VALUES (:groupId, :imienazwisko, :now, :zamowienie, :komentarz, :cena)
        ";
        $query = $this->db->prepare($sql);
        $query->bindValue(":groupId", $order->getGroupId());
        $query->bindValue(":imienazwisko", $order->getName());
        $query->bindValue(":zamowienie", $order->getOrder());
        $query->bindValue(":komentarz", $order->getComment());
        $query->bindValue(":cena", floatval(str_replace(",", ".", $order->getPrice())));
        $query->bindValue(":now", $order->getDate());
        return $query->execute();
    }

    /**
     * Returns sum of orders for each dish
     *
     * @param array $orders
     * @return array[string]
     */
    public function getSummary(array $orders) {
        $summary = array();
        foreach ($orders as $order) {
            /* @var $order Order */
            if ($order->getPrice() > 0) {
                foreach (explode("\n", $order->getOrder()) as $orderPosition) {
                    $unifiedOrder = mb_strtolower(strip_tags($orderPosition));
                    $unifiedOrder = preg_replace("/ +, +/", ", ", $unifiedOrder);
                    $unifiedOrder = trim($unifiedOrder);
                    $unifiedOrder = trim($unifiedOrder, ".");
                    if (empty($unifiedOrder)) {
                        continue;
                    }
                    $summary[$unifiedOrder] = isset($summary[$unifiedOrder]) ? $summary[$unifiedOrder] + 1 : 1;
                }
            }
        }
        return $summary;
    }

}
