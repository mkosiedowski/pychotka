<?php

class ShoutboxMessage {

    private $id;
    private $groupId;
    private $name;
    private $date;
    private $message;
    private $timestamp;

    function __construct($id, $groupId, $name, $date, $message, $timestamp, $timezone = "UTC") {
        $this->id = $id;
        $this->groupId = $groupId;
        $this->name = $name;
        $this->date = $date;
        $this->message = $message;
        $this->timestamp = strtotime($timestamp . " " . $timezone);
    }
    
    function getId() {
        return $this->id;
    }

    function getGroupId() {
        return $this->groupId;
    }

    function getName() {
        return $this->name;
    }

    function getDate() {
        return $this->date;
    }

    function getMessage() {
        return $this->message;
    }

    function getTimestamp($format = "Y-m-d H:i:s") {
        return date($format, $this->timestamp);
    }

    function setId($id) {
        $this->id = $id;
    }

    function setGroupId($groupId) {
        $this->groupId = $groupId;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setMessage($message) {
        $this->message = $message;
    }

    function setTimestamp($timestamp) {
        $this->timestamp = $timestamp;
    }

}
