<?php
namespace Pychotka;
class PychotkaParser {

    public function parse() {
        //exec("links -dump http://pychotka.gda.pl", $foo);
        $foo = @file("menu.txt");
        if($foo==false){
            echo '<span class="error">Menu file missing</span>';
            return array();
        }

        $menu = array();
        $i = 0;
        $write = false;
        foreach ($foo as $line) {
            $line = trim($line);
            if (empty($line)) {
                $write = true;
                if (!empty($menu[$i])) {
                    $i++;
                }
            } elseif ($write && !empty($line)) {
                $menu[$i][] = $line;
            }
        }

        $menuParsed = array();
        foreach ($menu as $menu_dzien) {
            $pychotkaMenu = new PychotkaMenu();
            $pychotkaMenu->setTitle(array_shift($menu_dzien));
            $pychotkaMenu->setMenu($this->parseMenu($menu_dzien));
            $menuParsed[] = $pychotkaMenu;
        }
        return $menuParsed;
    }

    private function parseMenu($menu) {
        $wynik = array();
        preg_match_all("/(.*?) ([0-9,]+) *zł./m", join(" ", $menu), $all);
        if (is_array($all[0])) {
            $ile = count($all[0]);
            for ($i = 0; $i < $ile; $i++) {
                $danie = new \MenuPosition();
                $danie->setName(trim($all[1][$i]));
                $danie->setPrice(trim($all[2][$i]));
                $wynik[] = $danie;
            }
        }

        return $wynik;
    }

}
