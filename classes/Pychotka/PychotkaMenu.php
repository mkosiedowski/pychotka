<?php
namespace Pychotka;
class PychotkaMenu implements \Menu {

    private $title;

    /**
     *
     * @var array
     */
    private $menu;

    private $constantMenu = array(
        'Pierogi z mięsem (8 szt.)' => '9',
        'Pierogi z kapustą i grzybami (8 szt.)' => '8',
        'Pierogi z kaszą i przeczarkami (8 szt.)' => '8',
        'Pierogi ruskie (8 szt.)' => '8',
        'Pierogi z serem (8 szt.)' => '8',
        'Pierogi z jagodami (8 szt.)' => '9',
        'Pierogi ze szpinakiem (8 szt.)' => '8',
        'Nalesniki z twarogiem (3 szt.)' => '7',
        'Placki ziemniaczane (4 szt.)' => '7',
        'Kotlet panierowany z indyka, frytki, surówki' => '12',
        'Kotlet schabowy, frytki, surówki' => '12',
        'De Volaille, frytki, surówki' => '12',
        'Corden Bleu, frytki, surówki' => '12',
        'Kurczak pieczony, frytki, surówki' => '12',
    );

    public function getTitle() {
        return $this->title;
    }

    public function getMenu() {
        return $this->menu;
    }

    public function setTitle($tytul) {
        $this->title = $tytul;
    }

    public function setMenu(array $menu) {
        $this->menu = $menu;
    }

    function getConstantMenu() {
        return $this->constantMenu;
    }

    public function getExtras() {
        return array();
    }
}
