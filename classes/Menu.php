<?php

interface Menu {

    public function getTitle();

    public function getMenu();

    public function getConstantMenu();

    public function getExtras();
}
