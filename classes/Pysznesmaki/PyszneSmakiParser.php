<?php
namespace PyszneSmaki;
class PyszneSmakiParser {

    public function parse() {
        $path = 'menu_pyszne_smaki.txt';
        if (!file_exists($path) || filemtime($path) < strtotime('-30 minutes')) {
            exec("links -dump -width 300 https://m.facebook.com/Pyszne-Smaki-Catering-1750569911876443/ > $path");
        }
        $foo = @file($path);
        if($foo==false){
            echo '<span class="error">Menu file missing</span>';
            return array();
        }

        $menu = array();
        $i = 0;
        $write = false;
        foreach ($foo as $line) {
            $line = trim($line);
            if (preg_match('/\w+ \d{2}\.\d{2}\.\d{4}/', $line)) {
                $write = true;
                if (!empty($menu[$i])) {
                    $i++;
                }
            }
            if ($write && !empty($line)) {
                $menu[$i][] = $line;
            }
            if (preg_match('/Danie 2/', $line)) {
                break;
            }
        }

        $menuParsed = array();
        foreach ($menu as $menu_dzien) {
            $pychotkaMenu = new PyszneSmakiMenu();
            $pychotkaMenu->setTitle(array_shift($menu_dzien));
            $pychotkaMenu->setMenu($this->parseMenu($menu_dzien));
            $menuParsed[] = $pychotkaMenu;
        }
        return $menuParsed;
    }

    private function parseMenu($menu) {
        $wynik = array();
        foreach ($menu as $position) {
            $danie = new \MenuPosition();
            $danie->setName(trim($position));
            $danie->setPrice(preg_match('/Zupa/i', $position) ? 2 : 13);
            $wynik[] = $danie;
        }

        return $wynik;
    }

}
