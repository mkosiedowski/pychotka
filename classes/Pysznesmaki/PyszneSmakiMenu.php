<?php
namespace PyszneSmaki;

class PyszneSmakiMenu implements \Menu {

    private $title;

    /**
     *
     * @var array
     */
    private $menu = [];

    private $constantMenu = array(
        'Danie 1' => '13',
        'Danie 2' => '13',
        'Zupa w zestawie' => '2',
        'Sama zupa' => '5',
    );

    public function getTitle() {
        return $this->title;
    }

    public function getMenu() {
        return $this->menu;
    }

    public function setTitle($tytul) {
        $this->title = $tytul;
    }

    public function setMenu(array $menu) {
        $this->menu = $menu;
    }

    function getConstantMenu() {
        return $this->constantMenu;
    }

    public function getExtras() {
        return array();
    }
}
