<?php
use Pychotka\PychotkaMenu;
use Pychotka\PychotkaParser;
use SurfBurger\SurfBurgerMenu;
class AppController extends Controller\Controller {

    /**
     * DB handle
     * @var PDO
     */
    private $db = null;

    /**
     *
     * @var OrderService
     */
    private $orderService = null;

    /**
     *
     * @var ShoutboxService
     */
    private $shoutboxService = null;

    /**
     * Current date
     * @var string
     */
    private $date = "";

    private $groupId = "";

    public function __construct() {
        $this->date = date("Y-m-d");
        $this->db = new PDO("sqlite:./db.sqlite");
        $this->orderService = new OrderService($this->db);
        $this->orderService->createDB();
        $this->shoutboxService = new ShoutboxService($this->db);
        $this->shoutboxService->createDB();
    }

    public function runDefault() {
        header('Content-Type: text/html; charset=utf-8');
        $this->getGroupId();
        $orders = $this->orderService->getOrders($this->date, $this->groupId);

        $menu = null;

        switch($this->groupId){
            case "surfburger":
                $viewName = "SurfBurgerView";
                $menu = new SurfBurgerMenu();
                break;
            case "pysznesmaki":
                $viewName = "PyszneSmakiView";
                $menu = new \PyszneSmaki\PyszneSmakiMenu();
                $parser = new \PyszneSmaki\PyszneSmakiParser();
                $menu->setMenu($parser->parse());
                break;
            case "pychotka":
            default:
                $viewName = "PychotkaView";
                $menu = new PychotkaMenu();
                $parser = new PychotkaParser();
                $menu->setMenu($parser->parse());
        }

        $view['menuHolder'] = $menu;
        $view['orders'] = $orders;
        $view['groupId'] = $this->groupId;

        $this->setViewName($viewName);
        $this->setViewVariables($view);
    }

    public function runGetShoutboxMessages() {
        $this->getGroupId();
        $lastId = filter_input(INPUT_GET, 'lastId', FILTER_SANITIZE_NUMBER_INT) ?: 0;
        $messages = $this->shoutboxService->getMessages($this->date, $this->groupId, $lastId);
        $result = array();
        foreach ($messages as $message) {
            /* @var $message ShoutboxMessage */
            $msg = array(
                'id' => $message->getId(),
                'timestamp' => $message->getTimestamp("H:i"),
                'name' => $message->getName(),
                'message' => $message->getMessage(),
            );
            $result[] = $msg;
        }
        $this->returnJSON($result);
    }

    public function runGetOrders() {
        $this->getGroupId();
        $lastId = filter_input(INPUT_GET, 'lastId', FILTER_SANITIZE_NUMBER_INT) ?: 0;
        $orders = $this->orderService->getOrders($this->date, $this->groupId, $lastId);
        $sum = array_reduce($orders, function ($sum, $order) {
            return $sum + round($order->getPrice(), 2);
        }, 0);
        $allOrdersForSummary = array();
        if (!empty($orders)) {
            $allOrdersForSummary = $this->orderService->getOrders($this->date, $this->groupId);
        }
        $result = array(
            'orders' => $this->prepareOrdersToView($orders),
            'sum' => number_format(round($sum, 2), 2),
            'summary' => $this->orderService->getSummary($allOrdersForSummary),
            'date' => $this->date,
        );
        $this->returnJSON($result);
    }

    private function prepareOrdersToView($orders) {
        $result = array();
        foreach ($orders as $order) {
            /* @var $message Order */
            $orderView = array(
                'id' => $order->getId(),
                'uniqueId' => md5($order->getName()),
                'name' => $order->getName(),
                'date' => $order->getDate(),
                'order' => htmlspecialchars($order->getOrder()),
                'price' => number_format(round($order->getPrice(), 2), 2, ",", " "),
                'comment' => htmlspecialchars($order->getComment()),
            );
            $result[] = $orderView;
        }
        return $result;
    }


    protected function getGroupId() {
        $this->groupId = filter_input(INPUT_GET, 'groupId', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH & FILTER_FLAG_STRIP_LOW);
        $this->groupId = empty($this->groupId) ? 'default' : $this->groupId;
    }

    public function runSaveOrder() {
        $this->getGroupId();
        if (!empty($_POST) && isset($_POST['imienazwisko']) && isset($_POST['zamowienie']) && isset($_POST['cena'])) {
            $comment = filter_input(INPUT_POST, 'komentarz');
            $name = filter_input(INPUT_POST, 'imienazwisko');
            $order = filter_input(INPUT_POST, 'zamowienie');
            $price = filter_input(INPUT_POST, 'cena', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $pychotkaOrder = new Order(null, $this->groupId, $name, $order, $price, $comment, $this->date);
            $this->orderService->saveOrder($pychotkaOrder);
            setcookie("imienazwisko", $pychotkaOrder->getName(), strtotime("+3 years"));
            header("Location: /?groupId={$this->groupId}");
            exit;
        }
        $this->runDefault();
    }

    public function runSaveMessage() {
        $this->getGroupId();
        if (!empty($_POST) && isset($_POST['imienazwisko']) && isset($_POST['wiadomosc'])) {
            $name = filter_input(INPUT_POST, 'imienazwisko');
            $message = filter_input(INPUT_POST, 'wiadomosc');
            $shoutboxMessage = new ShoutboxMessage(null, $this->groupId, $name, $this->date, $message, null);
            $this->shoutboxService->saveMessage($shoutboxMessage);
            setcookie("imienazwisko", $name, strtotime("+3 years"));
            header("Location: /?groupId={$this->groupId}");
            exit;
        }
        $this->runDefault();
    }

}
