<?php
namespace SurfBurger;
class SurfBurgerMenu implements \Menu {

    private $title;
    /**
     *
     * @var array
     */
    private $data;
    private $mainMenu = array();
    private $dataPath = "data/surfburger/surfburger-menu.json";
    private $mondayPromoDataPath = "data/surfburger/surfburger-menu-pn-promo.json";
    private $constantMenu = array();

    function __construct() {
        if(date('D', time()) === 'Mon'){
            $jsonData = file_get_contents($this->mondayPromoDataPath);
        }else{
            $jsonData = file_get_contents($this->dataPath);
        }
        $this->data = json_decode($jsonData, true);
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($tytul) {
        $this->title = $tytul;
    }

    public function getMenu() {
        return $this->data["menu"]["main"];
    }

    public function setMenu(array $menu) {
        $this->menu = $menu;
    }

    function getConstantMenu() {
        return $this->constantMenu;
    }

    public function getExtras() {
        return $this->data["menu"]["extras"];
    }
}
