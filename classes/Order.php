<?php

class Order {

    private $id;
    private $groupId;
    private $name;
    private $order;
    private $price;
    private $date;
    private $comment;

    public function __construct($id, $groupId, $name, $order, $price, $comment, $date) {
        $this->id = $id;
        $this->groupId = $groupId;
        $this->name = $name;
        $this->order = $order;
        $this->price = $price;
        $this->date = $date;
        $this->comment = $comment;
    }

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function getOrder() {
        return $this->order;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getDate() {
        return $this->date;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setOrder($order) {
        $this->order = $order;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function setDate($date) {
        $this->date = $date;
    }
        
    function getComment() {
        return $this->comment;
    }

    function setComment($comment) {
        $this->comment = $comment;
    }

    function getGroupId() {
        return $this->groupId;
    }

    function setGroupId($groupId) {
        $this->groupId = $groupId;
    }

}
