<section id="zamowienia">
    <article id="formularz">
        <h2>Zamów jedzenie</h2>
        <form action="/?groupId=<?= $groupId ?>&action=saveOrder" method="post">
            <label>Imię i nazwisko: <input type="text" name="imienazwisko" id="form-imienazwisko" required size=40 value="<?= isset($_COOKIE["imienazwisko"]) ? htmlspecialchars($_COOKIE["imienazwisko"]) : "" ?>"></label><br>
            <label>Zamówienie: <br><textarea name="zamowienie" cols=100 rows=2 id="form-zamowienie"></textarea></label><br>
            <label>Komentarz: <br><textarea name="komentarz" cols=100 rows=2 id="form-komentarz"></textarea></label><br>
            <label>Cena: <input type="number" step="any" name="cena" size=5 value="0" min="0" id="form-cena"> zł</label><br>
            <input type="submit" value="Zapisz">
        </form>
    </article>
    <article id="lista-zamowien">
        <h2>Lista zamowień</h2>
        <ul id="wszystkie-zamowienia" data-groupId="<?= $groupId ?>">
        </ul>
        <h3>Suma: <span class="suma"></span> zł</h3>
        <h3>Telefon (Morena): <a href="tel:+48536700662">536 700 662</a> </h3>
        <p>Do zamówienia za mniej niż 50zł należy doliczyć <b>5zł</b> za dowóz</p>
    </article>
    <script id="orderTmpl" type="text/html">
        <li id="zamowienie-{{uniqueId}}">
            <div class="zamowienieDane">
                <b class="zamowienieNazwisko">{{name}}</b>:<br>
                <div class="zamowienieTresc">{{{order}}}</div>
                <div class="zamowienieKomentarz">{{{comment}}}</div>
                <label id="{{uniqueId}}" class="zamowienieCena {{done}}">
                    <span class="cena"><b>Cena</b>: <sum class="zamowienieSuma">{{price}}</sum> zł</span>
                    <input type="checkbox" class="done" name="zamowienie-{{uniqueId}}" id="done-{{uniqueId}}">
                    <span class="zaplacone">zapłacone</span>
                </label>
            </div>
            <div class="zamowienieEdycja">Edytuj</div>
        </li>
    </script>
</section>
<section id="podsumowanie">
    <h2>Podsumowanie zamówienia:</h2>
    <ul></ul>
    <h3>Suma: <span class="suma"></span> zł</h3>
    <p><meter id="meter" value="0" low="30" optimum="30" min="0" max="150"></meter></p>
    <h3>Zamawia: <span id="zamawiajacy"></span></h3>
    <script id="summary" type="text/html">
        <li>{{dish}} - {{count}} raz{{#multiple}}y{{/multiple}}</li>
    </script>
</section>