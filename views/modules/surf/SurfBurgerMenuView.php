<section id="menu">
    <h1>Menu</h1>
    <dl>
        <form id="bun-form">
        <dd>Bułka:</dd>
        <?php foreach($menuHolder->getExtras()['buns'] as $index => $bun):?>
        <dd><input type="radio" name="bun" value="<?php echo $bun ?>" <?php if($index==0){echo "checked=\"checked\"";}?> /><?php echo ucfirst($bun) ?></dd>
        <?php endforeach; ?>
        </form>
    </dl>
    <dl>
        <dd>
            <ul>
                <?php foreach($menuHolder->getMenu() as $menuItem): ?>
                    <li data-nazwa="<?php echo htmlspecialchars($menuItem["name"]); ?>"
                        data-cena="<?php echo $menuItem['price'] ?>" class="danie">
                        <?php echo $menuItem["name"] ?> - <b><?php echo $menuItem['price'] ?></b> zł<br>
                    </li>
                <?php endforeach; ?>
            </ul>
        </dd>
    </dl>
</section>