<section id="shoutbox">
    <h2>Shoutbox</h2>
    <div id="shoutboxMessages" data-groupId="<?= $groupId ?>">
    </div>
    <form method="POST" action="/?groupId=<?= $groupId ?>&action=saveMessage">
        <label class="wiadomosc">Wiadomość: <input type="text" name="wiadomosc"></label>
        <label class="imienazwisko"> Podpis: <input type="text" name="imienazwisko" value="<?= isset($_COOKIE["imienazwisko"]) ? htmlspecialchars($_COOKIE["imienazwisko"]) : "" ?>"></label>
        <div class="submit"><input type="submit" value="wyślij"></div>
    </form>
    <script id="shoutboxmsg" type="text/html">
        <div class="shoutboxMessage">
            [{{ timestamp }}]
            <b>{{ name }}</b>:
            {{ message }}
        </div>
    </script>
</section>
