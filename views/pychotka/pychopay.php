<?php if (count($orders) > 1): ?>
    <section id="pychopay">
        <table>
            <caption>
                <h1>Pychopay&nbsp;<sup>&beta;</sup></h1>
            </caption>
            <thead>
                <tr>
                    <th>&ang;</th>
                    <th>Gotówka</th>
                    <?php foreach ($orders as $payer): ?>
                        <th><?= $payer->getName() ?></th>
                    <?php endforeach; ?>
                    <th>Reszta</th>
                </tr>
            </thead>
            <tbody data-pychopay>
                <?php foreach ($orders as $payer): ?>
                    <?php $payerKey = $payer->getName() ?>
                    <tr data-payer="<?= htmlspecialchars($payerKey) ?>">
                        <th><?= htmlspecialchars($payerKey) ?></th>
                        <td><input type="number" data-cash="<?= htmlspecialchars($payerKey) ?>"/></td>
                        <?php foreach ($orders as $payee): ?>
                            <?php $payeeKey = $payee->getName() ?>
                            <?php $payment = round($payee->getPrice(), 2) ?>

                            <td><input id="<?= htmlspecialchars($payerKey) ?>paysFor<?= htmlspecialchars($payeeKey) ?>" type="checkbox" data-payer="<?= htmlspecialchars($payerKey) ?>" data-payee="<?= htmlspecialchars($payeeKey) ?>" data-payment="<?= $payment ?>"/><label for="<?= htmlspecialchars($payerKey) ?>paysFor<?= htmlspecialchars($payeeKey) ?>"></label></td>
                            <?php endforeach; ?>
                        <td><input type="number" data-change="Wojtek" readonly/></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </section>
<?php endif; ?>