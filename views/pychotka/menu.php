<section id="menu">
    <h1>Menu</h1>
    <dl>
        <?php foreach ($menuHolder->getMenu() as $menuDzien): ?>
            <dt><?= $menuDzien->getTitle() ?></dt>
            <dd>
                <ul>
                    <?php foreach ($menuDzien->getMenu() as $danie): ?>
                        <li data-nazwa="<?= htmlspecialchars($danie->getName()) ?>" data-cena="<?= str_replace(",", ".", $danie->getPrice()) ?>" class="danie">
                            <?= $danie->getName() ?> - <b><?= $danie->getPrice() ?></b> zł<br>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </dd>
        <?php endforeach; ?>
    </dl>
    <h2>Menu stałe</h2>
    <ul>
        <?php foreach ($menuHolder->getConstantMenu() as $nazwa => $cena): ?>
            <li data-nazwa="<?= $nazwa ?>" data-cena="<?= $cena ?>" class="danie"><?= $nazwa ?> - <b><?= $cena ?></b> zł</li>
            <?php endforeach; ?>
    </ul>
</section>