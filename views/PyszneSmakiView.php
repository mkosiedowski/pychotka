<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Pychotka</title>
    <link rel="stylesheet" href="./css/style.css">
    <script src="./js/jbone.min.js"></script>
    <script src="./js/ICanHaz.min.js"></script>
    <script src="./js/cm.js"></script>
    <script src="./js/shoutbox.js"></script>
    <script src="./js/pychotkaOrders.js"></script>
    <script src="./js/desktop-notify.min.js"></script>
</head>
<body>
<?php include('modules/TopBarView.php'); ?>
<?php include('pychotka/menu.php'); ?>
<?php include('modules/pysznesmaki/zamowienia.php'); ?>
<?php include('pychotka/shoutbox.php'); ?>
<script src="./js/pychotka.js"></script>
</body>
</html>
